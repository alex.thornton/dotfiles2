# Installation

1. Clone this repo to ~/Projects/dotfiles
1. Install homebrew: https://brew.sh/
1. Install apps with homebrew: `source brew.sh`
1. Install oh-my-zsh: https://github.com/ohmyzsh/ohmyzsh
1. Set up symlinks:
    1. `ln -s ~/Projects/dotfiles/.zshrc ~/.zshrc`
    1. `ln -s ~/Projects/dotfiles/.gitconfig ~/.gitconfig`
