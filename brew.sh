#!/usr/bin/env zsh

alias brew='arch -x86_64 brew'

brew update
brew upgrade

brew tap "homebrew/cask"
brew tap "homebrew/cask-versions"
brew tap "homebrew/cask-drivers"
brew tap "homebrew/core"
brew install "awscli"
brew install "curl"
brew install "node"
brew install "nodenv"
brew install "pyenv"
brew install "python"
brew install "dbeaver-community"
brew install "docker"
brew install "google-chrome"
brew install "iterm2"
brew install "keepingyouawake"
brew install "logitech-unifying"
brew install "microsoft-excel"
brew install "microsoft-office"
brew install "microsoft-powerpoint"
brew install "postman"
brew install "rectangle"
brew install "slack"
brew install "spotify"
brew install "the-clock"
brew install "visual-studio-code"
brew install "zoom"
